package com.janakagamini.animations;

/**
 * Sample data object
 */
public class Item {

    private String title;
    private String subtitle;
    private String distance;
    private String imageUri;

    public Item(String title, String subtitle, String distance, String imageUri) {
        this.title = title;
        this.subtitle = subtitle;
        this.distance = distance;
        this.imageUri = imageUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
