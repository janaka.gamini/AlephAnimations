package com.janakagamini.animations;

/**
 * Corresponding View model for Item class.
 */
public class ViewItem {

    private Item item;
    private boolean shouldExpand;

    public ViewItem(Item item, boolean shouldExpand) {
        this.item = item;
        this.shouldExpand = shouldExpand;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean shouldExpand() {
        return shouldExpand;
    }

    public void setShouldExpand(boolean shouldExpand) {
        this.shouldExpand = shouldExpand;
    }
}