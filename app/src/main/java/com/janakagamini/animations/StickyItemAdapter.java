package com.janakagamini.animations;

import android.content.Context;
import android.os.Handler;
import android.support.transition.Scene;
import android.support.transition.TransitionManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;
import java.util.List;

/**
 * An adapter that has two states, expanded and collapsed. When expanded, it will display all items.
 * When in collapsed mode, it will only show the sticky items.
 */
public class StickyItemAdapter extends RecyclerView.Adapter<StickyItemAdapter.ItemHolder> {

    private Context context;

    final private List<Item> allItems; // full list of items.
    private List<ViewItem> items; // items currently being displayed.
    private int stickyCount; // number of items to persist at top.

    private RequestManager glide;

    private boolean expanded = false; // default state of the adapter is to be collapsed.

    private int interval = 60; // animation interval

    public StickyItemAdapter(Context context, List<Item> items, int stickyCount) {
        this.context = context;
        this.allItems = items;
        this.stickyCount = stickyCount;
        this.items = new ArrayList<>();

        this.glide = Glide.with(context);

        for (int i = 0; i < stickyCount; i++) {
            this.items.add(new ViewItem(allItems.get(i), false));
        }
    }

    @Override
    public StickyItemAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(final StickyItemAdapter.ItemHolder holder, int position) {

        final Item item = items.get(position).getItem();
        final boolean shouldExpand = items.get(position).shouldExpand();
        holder.setItemView(item, glide);

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                holder.setItemView(item, glide);
            }
        };

        final Scene collapsedScene = Scene.getSceneForLayout(holder.viewGroup,
                R.layout.item_view_collapsed,
                context);

        final Scene expandedScene = Scene.getSceneForLayout(holder.viewGroup,
                R.layout.item_view_expanded,
                context);

        collapsedScene.setEnterAction(runnable);
        expandedScene.setEnterAction(runnable);

        if (shouldExpand) {
            TransitionManager.go(expandedScene,
                    TransitionUtils.getExpandingTransitionSet());
        } else {
            TransitionManager.go(collapsedScene,
                    TransitionUtils.getCollapsingTransitionSet());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    /**
     * Change adapter state to expanded
     */
    public void expand() {
        if (!expanded) {
            for (int i = 0; i < allItems.size(); i++) {
                if (i < stickyCount) {
                    expand(i, false, i * interval);
                    continue;
                }
                expand(i, true, i * interval);
            }
        }
        expanded = true;
    }

    /**
     * Set each item view as expanded
     *
     * @param position  - Item position in adapter
     * @param shouldAdd - For non sticky items, they need to be added to the items list
     * @param delay     - Animation delay
     */
    private void expand(final int position,
                        final boolean shouldAdd,
                        int delay) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldAdd) {
                    items.get(position).setShouldExpand(true);
                    notifyItemChanged(position);
                } else {
                    items.add(position, new ViewItem(allItems.get(position), true));
                    notifyItemInserted(position);
                }
            }
        }, delay);
    }

    /**
     * Change adapter state to collapsed. Note collapsing happens in reverse order.
     */
    public void collapse() {
        if (expanded) {
            for (int i = allItems.size() - 1; i >= 0; i--) {
                if (i < stickyCount) {
                    collapse(i, false, (allItems.size() - i) * interval);
                    continue;
                }
                collapse(i, true, (allItems.size() - i) * interval);
            }
        }
        expanded = false;
    }

    /**
     * Set item view as collapsed.
     *
     * @param position     - Item position in adapter
     * @param shouldRemove - For non sticky items, they need to be removed from items list.
     * @param delay        - Animation delay
     */
    private void collapse(final int position, final boolean shouldRemove, final int delay) {

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (shouldRemove) {
                    items.remove(position);
                    notifyItemRemoved(position);
                } else {
                    items.get(position).setShouldExpand(false);
                    notifyItemChanged(position);
                }
            }
        }, delay);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        View itemView;
        ViewGroup viewGroup;

        public ItemHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.viewGroup = (ViewGroup) itemView;
        }

        // Convenience method to set the contents of the view.
        public void setItemView(Item item, RequestManager glide) {
            ImageView iv_thumb = viewGroup.findViewById(R.id.thumb);
            TextView tv_title = viewGroup.findViewById(R.id.title);
            TextView tv_subtitle = viewGroup.findViewById(R.id.subtitle);
            TextView tv_distance = viewGroup.findViewById(R.id.distance);

            if (iv_thumb != null) {
                glide.load(item.getImageUri())
                        .into(iv_thumb);
            }

            tv_title.setText(item.getTitle());

            if (tv_subtitle != null) {
                tv_subtitle.setText(item.getSubtitle());
            }

            tv_distance.setText(item.getDistance());
        }
    }


}
