package com.janakagamini.animations;

import android.support.v7.widget.RecyclerView;

import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;

public class CustomFadeInLeftAnimator extends FadeInLeftAnimator {

    /**
     * We only require fade-in-left animation to be used in the event of `notifyItemInserted(...)`
     * and `notifyItemRemoved(...), overriding here to disable this animation for `notifyItemChanged(...)`
     */
    @Override
    public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder, int fromX, int fromY, int toX, int toY) {
        //return super.animateChange(oldHolder, newHolder, fromX, fromY, toX, toY);
        return true;
    }
}
