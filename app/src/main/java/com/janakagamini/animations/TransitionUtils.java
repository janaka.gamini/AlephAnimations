package com.janakagamini.animations;

import android.support.transition.ChangeBounds;
import android.support.transition.ChangeImageTransform;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.transition.TransitionSet;

/**
 * Helper class for Transitions.
 */
public class TransitionUtils {

    public static TransitionSet getExpandingTransitionSet() {

        Transition changeBounds = new ChangeBounds();
        Transition fade = new Fade(Fade.IN);
        fade.addTarget(R.id.thumb);
        fade.addTarget(R.id.subtitle);
        fade.setStartDelay(changeBounds.getDuration());
        TransitionSet expand = new TransitionSet();
        expand.addTransition(changeBounds);
        expand.addTransition(fade);

        return expand;
    }

    public static TransitionSet getCollapsingTransitionSet() {

        Transition imageTransform = new ChangeImageTransform();
        imageTransform.addTarget(R.id.thumb);
        Transition fade = new Fade(Fade.OUT);
        fade.addTarget(R.id.thumb);
        fade.addTarget(R.id.subtitle);
        Transition changeBounds = new ChangeBounds();
        changeBounds.setStartDelay(fade.getDuration());
        TransitionSet collapse = new TransitionSet();
        collapse.addTransition(imageTransform);
        collapse.addTransition(fade);
        collapse.addTransition(changeBounds);

        return collapse;
    }
}
