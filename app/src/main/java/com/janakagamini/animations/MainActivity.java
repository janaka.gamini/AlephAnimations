package com.janakagamini.animations;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_DRAGGING;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.items_view)
    RecyclerView rv_itemsView;
    StickyItemAdapter itemAdapter;
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    boolean isBottomNavigationHidden = true;

    int stickyCount = 2; // How many items should be visible in collapsed state.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initToolbar();
        initItemsView();

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setShowHideAnimationEnabled(true);
            getSupportActionBar().hide();
        }
    }

    private void initItemsView() {
        linearLayoutManager = new LinearLayoutManager(this);
        rv_itemsView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_itemsView.getContext(),
                linearLayoutManager.getOrientation());
        rv_itemsView.addItemDecoration(dividerItemDecoration);
        rv_itemsView.setItemAnimator(new CustomFadeInLeftAnimator());

        itemAdapter = new StickyItemAdapter(this, initItems(), stickyCount);
        itemAdapter.setHasStableIds(true);
        rv_itemsView.setAdapter(itemAdapter);

        // Expand items list when user is pulling down.
        // TODO Both drag down and drag up events are captured here. Need to refine to only capture drag down.
        rv_itemsView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState) {
                    case SCROLL_STATE_DRAGGING:
                        expandList();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    // Just some random data
    private List<Item> initItems() {
        List<Item> items = new ArrayList<>();

        items.add(new Item("Microwave Oven", "Electronics", "126m", "file:///android_asset/microwave_oven.jpg"));
        items.add(new Item("Television", "Electronics", "498m", "file:///android_asset/television.jpg"));
        items.add(new Item("Vacuum Cleaner", "Electronics", "34m", "file:///android_asset/vacuum_cleaner.jpg"));
        items.add(new Item("Table", "Furniture", "300m", "file:///android_asset/table.jpg"));
        items.add(new Item("Chair", "Furniture", "99m", "file:///android_asset/chair.jpg"));
        items.add(new Item("Almirah", "Furniture", "600m", "file:///android_asset/almirah.jpg"));

        return items;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                collapseList();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Expand list:
     * 1) Show toolbar
     * 2) Expand items list
     * 3) Show bottom navigation
     */
    private void expandList() {
        if (getSupportActionBar() != null && !getSupportActionBar().isShowing()) {
            getSupportActionBar().show();
        }
        itemAdapter.expand();
        showBottomNavigation();
    }

    /**
     * Collapse list:
     * 1) Hide bottom navigation
     * 2) Scroll to first item and Collapse items list
     * 3) Hide toolbar
     */
    private void collapseList() {
        hideBottomNavigation();
        rv_itemsView.smoothScrollToPosition(0);
        itemAdapter.collapse();
        if (getSupportActionBar() != null && getSupportActionBar().isShowing()) {
            getSupportActionBar().hide();
        }
    }

    private void showBottomNavigation() {
        if (isBottomNavigationHidden) {
            Animation slideUp = AnimationUtils.loadAnimation(this,
                    R.anim.slide_up);
            bottomNavigation.startAnimation(slideUp);
            bottomNavigation.setVisibility(View.VISIBLE);
        }
        isBottomNavigationHidden = false;
    }

    private void hideBottomNavigation() {
        if (!isBottomNavigationHidden) {
            Animation slideDown = AnimationUtils.loadAnimation(this,
                    R.anim.slide_down);
            bottomNavigation.startAnimation(slideDown);
            bottomNavigation.setVisibility(View.INVISIBLE);
        }
        isBottomNavigationHidden = true;
    }
}
