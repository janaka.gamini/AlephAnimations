# Aleph Labs - Assignment 

- Develop the following with best use of Android Technologies for Android phones. Target Android version will be 2.3 and above. You can refer the attached sample video.
- Create a list of items and show only first 2 on top
- User should able to pull down the list
- As the list comes down list items will be expanded to a detailed state (with subtitle and thumbnail)
- Each new list item will fade in and slide in from the left
- After the list has fully covered the screen filter tabs (top and bottom bar) will slide up in from the bottom with ease
- Add back button on the top header
- Tap on back button to bring back the drop down list to the original/first state where you can see two list items, the animation performs in reverse direction.
- Focus more on the Animation part

Note: You can use your own assets but try to match with design as much as possible.
 
# Solution - Aleph Animations

- Developed using Android Studio 2.3.3
- Clone the repo and open the `settings.gradle` file in Android Studio.

## Notes

- Assumed it is safe to drop the minimum requirement for Android 2.3. Using `minSdkVersion` from the
 Android appCompat libraries instead, which is `14` (Android 4+). This covers ~99.95% of Android devices
 active today [[Ref](https://data.apteligent.com/android/)].
- Tested only on virtual device with API level 16, but should work all the way down to 14.
- Haven't focused on re-creating purely aesthetic aspects like colours, dimensions etc. since there
aren't any specs/resources for that. Focused more on re-creating the animations.
- Haven't focused on writing unit tests etc. since there are no functional requirements.

## Known Issues

- List expands even if the user drags up.